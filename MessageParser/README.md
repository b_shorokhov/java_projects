# Message parser

This is a simple text messages parser that is expected to work in the same way as [HipChat](https://www.hipchat.com) does.

## Requirements
Java 8

## Build
To build app run simply `mvn clean install`. It will build sources, run tests and generate jar file to test manually.

## Launch
To launch app it is necessary to do 2 steps:

1. `cd /path-to-project-root/target`
2. `java -jar hipchat-1.0-SNAPSHOT-jar-with-dependencies.jar "your string message to test"`

## Logic
While parsing message next rules are applied:

* Message is being split into words using whitespace as the separator, preserving all tokens, including empty tokens created by adjacent separators.
* Iterate through obtained words
* Word is **Mention** if it starts with `@`. Non-literals in the end are erased.
* Word is **Emoticon** if it starts with `(` and ends with `)`. if it is followed by text, other special chars except whitespace then it is not emoticon. It is just text.
* Word is **Link** if it starts with `http://`, `https://`, `file://`, `ftp://`. as usual messengers do it is not checked how link ends, so for example:
    * "Preceding text http://test.link.com succeeding text" is correct.
    * "Preceding text http://test.link.com. Succeeding text" is correct. Even if it would contain dot in the end
    * "Preceding text http://test.link.com.Suceeding text" is wrong as link then would contain extra text in the end