package utils;

import entity.Link;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author: bshorokhov
 */
public class WebUtilsTest
{
    private static final String CORRECT_URL_STR = "http://sportevent.com.ua/events/race-nation-odessa-night/";
    private static final String CORRECT_PAGE_TITLE = "Ukraine Sport Events - Спортивные мероприятия Украины";
    private static final String PAGE_NOT_FOUND_URL = "http://sportevent.com.ua/events/race-nation-odessa-night/.extratext";
    private static final String PAGE_TITLE_FOR_PAGE_NOT_FOUND_URL = "`Регистрация на`$ITEM.name";

    @Test
    public void getLinkFromStringUrl() throws Exception
    {
        Link correctLink = WebUtils.getLinkFromStringUrl(CORRECT_URL_STR);
        assertEquals(CORRECT_URL_STR, correctLink.getUrl());
        assertEquals(CORRECT_PAGE_TITLE, correctLink.getTitle());

        Link extpageNotFoundLink = WebUtils.getLinkFromStringUrl(PAGE_NOT_FOUND_URL);
        assertEquals(PAGE_NOT_FOUND_URL, extpageNotFoundLink.getUrl());
        assertNotNull(extpageNotFoundLink.getTitle());
        assertEquals(PAGE_TITLE_FOR_PAGE_NOT_FOUND_URL, extpageNotFoundLink.getTitle());
    }

}