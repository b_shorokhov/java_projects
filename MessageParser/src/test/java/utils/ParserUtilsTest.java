package utils;

import entity.Emoticon;
import entity.Link;
import entity.Mention;
import entity.Message;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * @author: bshorokhov
 */
public class ParserUtilsTest
{
    private static final String MENTION_USERNAME = "username";
    private static final String MENTION_SOMEBODY_ELSE = "somebody_else";

    private static final String EMOTICON_AWESOME = "awesome";
    private static final String EMOTICON_SADCOWBOY = "sadcowboy";
    private static final String EMOTICON_INCORRECT = "what&is~that?";

    private static final String LINK_RUN_EVENT = "http://sportevent.com.ua/events/race-nation-odessa-night/";
    private static final String LINK_CINEMA_EVENT = "https://www.timeout.com/newyork/movies/rooftop-cinema-club-calendar";

    private static final String PAGE_TITLE_RUN_EVENT = "Ukraine Sport Events - Спортивные мероприятия Украины";
    private static final String PAGE_TITLE_CINEMA_EVENT = "Rooftop Cinema Club full calendar of outdoor movie screenings in NYC";

    private static final String STR_WITH_SINGLE_PARAMS = "Hey, @" + MENTION_USERNAME + "!!! Look at this (" + EMOTICON_AWESOME + ") event: " + LINK_RUN_EVENT + "!!!";
    private static final String STR_WITH_MANY_PARAMS = "Hey, @" + MENTION_USERNAME + ", Look at this (" + EMOTICON_AWESOME + ") event: " + LINK_RUN_EVENT + "!!! @" + MENTION_SOMEBODY_ELSE + " said he wanted to participate but he is busy with another event: " + LINK_CINEMA_EVENT + ". And it is (" + EMOTICON_SADCOWBOY + ")";
    private static final String STR_WITH_EMPTY_PARAMS = "There is no params to parse in this message";
    private static final String STR_WITH_HIDDEN_PARAMS = "Hey, text@" + MENTION_USERNAME + "!text. Look at this (" + EMOTICON_INCORRECT + ") link... Seems it not worse any time waste..." + LINK_RUN_EVENT;

    private static Message testMessage;

    private static JSONObject expectedMessageAsJson;

    @Before
    public void setUp() throws Exception
    {
        String testMessageStr = "This is just a test message.";

        Set<Mention> mentions = new TreeSet<>();
        mentions.add(new Mention(MENTION_USERNAME));
        mentions.add(new Mention(MENTION_SOMEBODY_ELSE));

        Set<Emoticon> emoticons = new TreeSet<>();
        emoticons.add(new Emoticon(EMOTICON_AWESOME));
        emoticons.add(new Emoticon(EMOTICON_SADCOWBOY));

        Set<Link> links = new LinkedHashSet<>();
        links.add(new Link(LINK_RUN_EVENT, PAGE_TITLE_RUN_EVENT));
        links.add(new Link(LINK_CINEMA_EVENT, PAGE_TITLE_CINEMA_EVENT));

        testMessage = new Message(testMessageStr, mentions, emoticons, links);
        expectedMessageAsJson = new JSONObject(testMessage);

    }

    @After
    public void tearDown() throws Exception
    {
        testMessage = null;
    }

    @Test
    public void parseStringToMessage() throws Exception
    {
        Message messageWithSingleParams = ParserUtils.parseStringToMessage(STR_WITH_SINGLE_PARAMS);
        assertEquals(messageWithSingleParams.getMentions().size(), 1);
        assertEquals(messageWithSingleParams.getEmoticons().size(), 1);
        assertEquals(messageWithSingleParams.getLinks().size(), 1);

        Message messageWithManyParams = ParserUtils.parseStringToMessage(STR_WITH_MANY_PARAMS);
        assertEquals(messageWithManyParams.getMentions().size(), 2);
        assertEquals(messageWithManyParams.getEmoticons().size(), 2);
        assertEquals(messageWithManyParams.getLinks().size(), 2);

        Message messageWithEmptyParams = ParserUtils.parseStringToMessage(STR_WITH_EMPTY_PARAMS);
        assertEquals(messageWithEmptyParams.getMentions().size(), 0);
        assertEquals(messageWithEmptyParams.getEmoticons().size(), 0);
        assertEquals(messageWithEmptyParams.getLinks().size(), 0);

        Message messageWithHiddenParams = ParserUtils.parseStringToMessage(STR_WITH_HIDDEN_PARAMS);
        assertEquals(messageWithHiddenParams.getMentions().size(), 0);
        assertEquals(messageWithHiddenParams.getEmoticons().size(), 0);
        assertEquals(messageWithHiddenParams.getLinks().size(), 0);
    }

    @Test
    public void convertMessageToJson() throws Exception
    {
        JSONObject actualMessageAsJson = ParserUtils.convertMessageToJson(testMessage);
        assertEquals(actualMessageAsJson.getJSONArray("mentions").length(), 2);
        assertEquals(actualMessageAsJson.getJSONArray("emoticons").length(), 2);
        assertEquals(actualMessageAsJson.getJSONArray("links").length(), 2);
        assertEquals(expectedMessageAsJson.toString(), actualMessageAsJson.toString());
    }

}