package utils;

import entity.Link;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author: bshorokhov
 */
public class WebUtils
{
    public static Link getLinkFromStringUrl(String urlStr)
    {
        Link link = null;
        try (CloseableHttpClient httpClient = HttpClients.createDefault())
        {
            HttpGet httpGet = new HttpGet(urlStr);
            try (CloseableHttpResponse response = httpClient.execute(httpGet))
            {
                HttpEntity entity = response.getEntity();
                if (entity != null)
                {
                    try (InputStream input = entity.getContent())
                    {
                        String document = IOUtils.toString(input, "UTF-8");
                        String title = StringUtils.substringBetween(document, "<title>", "</title>");
                        String url = httpGet.getURI().toURL().toString();
                        link = new Link(url, title);
                    }
                }
            }
        }
        catch (IOException e)
        {
            return new Link();
        }
        return link;
    }
}