package utils;

import entity.Emoticon;
import entity.Link;
import entity.Mention;
import entity.Message;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author: bshorokhov
 */
public class ParserUtils
{

    public static Message parseStringToMessage(String messageStr)
    {
        Set<Mention> mentions = new TreeSet<>();
        Set<Emoticon> emoticons = new TreeSet<>();
        Set<Link> links = new LinkedHashSet<>();

        String[] words = StringUtils.splitPreserveAllTokens(messageStr);

        for (String word : words)
        {
            if (StringUtils.startsWith(word, "@"))
            {
                String mentionStr = StringUtils.replacePattern(word, "[^a-zA-Z0-9_]", "");
                Mention mention = new Mention(mentionStr);
                mentions.add(mention);
            }
            else if (StringUtils.startsWith(word, "(") && StringUtils.endsWith(word, ")"))
            {
                String emoticonStr = StringUtils.substringBetween(word, "(", ")");
                if (StringUtils.isAlphanumeric(emoticonStr))
                {
                    Emoticon emoticon = new Emoticon(emoticonStr);
                    emoticons.add(emoticon);
                }
            }
            else if (StringUtils.startsWithAny(word, "http://", "https://", "ftp://", "file://"))
            {
                Link link = WebUtils.getLinkFromStringUrl(word);
                links.add(link);
            }
        }

        return new Message(messageStr, mentions, emoticons, links);
    }

    public static JSONObject convertMessageToJson(Message message)
    {
        return new JSONObject(message);
    }

}
