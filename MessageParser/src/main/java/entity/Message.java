package entity;

import java.util.Set;

/**
 * @author: bshorokhov
 */
public class Message
{
    private String message;
    private Set<Mention> mentions;
    private Set<Emoticon> emoticons;
    private Set<Link> links;

    public Message()
    {
    }

    public Message(String message, Set<Mention> mentions, Set<Emoticon> emoticons, Set<Link> links)
    {
        this.message = message;
        this.mentions = mentions;
        this.emoticons = emoticons;
        this.links = links;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Set<Mention> getMentions()
    {
        return mentions;
    }

    public void setMentions(Set<Mention> mentions)
    {
        this.mentions = mentions;
    }

    public Set<Emoticon> getEmoticons()
    {
        return emoticons;
    }

    public void setEmoticons(Set<Emoticon> emoticons)
    {
        this.emoticons = emoticons;
    }

    public Set<Link> getLinks()
    {
        return links;
    }

    public void setLinks(Set<Link> links)
    {
        this.links = links;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Message message1 = (Message) o;

        if (!message.equals(message1.message))
        {
            return false;
        }
        if (!mentions.equals(message1.mentions))
        {
            return false;
        }
        if (!emoticons.equals(message1.emoticons))
        {
            return false;
        }
        return links.equals(message1.links);

    }

    @Override
    public int hashCode()
    {
        int result = message.hashCode();
        result = 31 * result + mentions.hashCode();
        result = 31 * result + emoticons.hashCode();
        result = 31 * result + links.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "Message{" +
                "message='" + message + '\'' +
                ", mentions=" + mentions +
                ", emoticons=" + emoticons +
                ", links=" + links +
                '}';
    }
}
