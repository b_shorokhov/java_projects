package entity;

/**
 * @author: bshorokhov
 */
public class Emoticon implements Comparable<Emoticon>
{
    String emoticon;

    public Emoticon()
    {
    }

    public Emoticon(String emoticon)
    {
        this.emoticon = emoticon;
    }

    public String getEmoticon()
    {
        return emoticon;
    }

    public void setEmoticon(String emoticon)
    {
        this.emoticon = emoticon;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Emoticon emoticon1 = (Emoticon) o;

        return emoticon.equals(emoticon1.emoticon);

    }

    @Override
    public int hashCode()
    {
        return emoticon.hashCode();
    }

    @Override
    public String toString()
    {
        return "Emoticon{" +
                "emoticon='" + emoticon + '\'' +
                '}';
    }


    @Override
    public int compareTo(Emoticon o)
    {
        return this.emoticon.compareTo(o.getEmoticon());
    }
}
