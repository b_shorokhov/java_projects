package entity;

/**
 * @author: bshorokhov
 */
public class Link
{
    String url;
    String title;

    public Link()
    {
    }

    public Link(String url, String title)
    {
        this.url = url;
        this.title = title;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Link link = (Link) o;

        if (!url.equals(link.url))
        {
            return false;
        }
        return title.equals(link.title);

    }

    @Override
    public int hashCode()
    {
        int result = url.hashCode();
        result = 31 * result + title.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "Link{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

}
