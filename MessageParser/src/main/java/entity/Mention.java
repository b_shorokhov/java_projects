package entity;

/**
 * @author: bshorokhov
 */
public class Mention implements Comparable<Mention>
{
    String mention;

    public Mention()
    {
    }

    public Mention(String mention)
    {
        this.mention = mention;
    }

    public String getMention()
    {
        return mention;
    }

    public void setMention(String mention)
    {
        this.mention = mention;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Mention mention1 = (Mention) o;

        return mention.equals(mention1.mention);

    }

    @Override
    public int hashCode()
    {
        return mention.hashCode();
    }

    @Override
    public String toString()
    {
        return "Mention{" +
                "mention='" + mention + '\'' +
                '}';
    }

    @Override
    public int compareTo(Mention o)
    {
        return this.mention.compareTo(o.getMention());
    }
}
