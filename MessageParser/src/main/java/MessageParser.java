import entity.Message;
import utils.ParserUtils;

/**
 * @author: bshorokhov
 */
public class MessageParser
{
    private static final int INDENT_FACTOR = 4;

    public static void main(String[] args)
    {
        String messageStr;
        if (args.length == 0)
        {
            throw new IllegalArgumentException("No arguments received");
        }
        else if (args.length > 1)
        {
            throw new IllegalArgumentException("Too many arguments received");
        }
        else
        {
            messageStr = args[0];
        }

        System.out.println("Input: \"" + messageStr + "\"");
        System.out.println("Return (string):");

        Message message = ParserUtils.parseStringToMessage(messageStr);
        String messageAsJson = ParserUtils.convertMessageToJson(message).toString(INDENT_FACTOR);

        System.out.println(messageAsJson);
    }

}
