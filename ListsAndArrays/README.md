# ListsAndArrays

This small project contains different implementations of linked list data structure and checks array if it is Special. Special here means that array has a "pivot" index and sum of elements at left equals to the sum of elements at right.

## Requirements
* Java 8
* Maven 3

## Build
To build app run simply `mvn clean install` or `mvn clean package`. 

## Usage
There is an example class `DemoApp` that shows how to simply work with different implemented variations of list data structure and arrays checks.
Feel free to use or extend it according to your needs.

## Content
* class `DemoApp` - client example
* package `array` - contains class that checks if  array is special
* package `cuteLists` - contains mine vision of how according to best practices list data structure might look like.
 There is a **List** interface with really basic set of methods and it's 2 implementations: **LinkedList** and **DoublylinkedList**
* package `rwlist` - contains interface **RWNode** for nodes that can define a linked list, it's implementing class, iterator class to iterate nodes forward and printer class that can do print of such list and also reverse print without usage of any kind of looping. 

## Test coverage
![TestCoverage](TestCoverage.png)
