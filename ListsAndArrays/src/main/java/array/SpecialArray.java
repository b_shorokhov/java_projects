package array;

/**
 * @author: bshorokhov
 */
public final class SpecialArray
{

    private static final int SPECIAL_ARRAY_MIN_LENGTH = 3;

    public static boolean isSpecial(int[] array)
    {
        if(array.length < SPECIAL_ARRAY_MIN_LENGTH) {
            throw new IllegalArgumentException("Array has insufficient length to answer if it is a special array.");
        }

        int sumAtLeft = array[0];
        int sumAtRight = 0;

        for (int element : array)
        {
            sumAtRight += element;
        }

        for (int i = 0; i < array.length - 1; i++)
        {
            if (sumAtLeft == sumAtRight)
            {
                return true;
            }
            sumAtLeft += array[i + 1];
            sumAtRight -= array[i];
        }

        return false;
    }
}
