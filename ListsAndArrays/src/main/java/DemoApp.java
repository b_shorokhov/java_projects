import array.SpecialArray;
import cute_lists.List;
import cute_lists.impl.DoublyLinkedList;
import cute_lists.impl.LinkedList;
import rwlist.RWNode;
import rwlist.RWNodeImpl;
import rwlist.RWPrinter;

import java.util.Arrays;

/**
 * @author: bshorokhov
 * <p>
 * This class is only an example of how to use existing datastructures
 * and might not be considered as user interface or something else.
 */
public class DemoApp
{
    public static void main(String[] args)
    {
        testLinkedList();
        testDoublyLinkedList();
        testRWNodeList();
        testSpesialArray();
    }

    private static void testLinkedList()
    {
        List<String> linkedList = new LinkedList<>();
        linkedList.add("item_1");
        linkedList.add("item_2");
        linkedList.add("item_3");
        linkedList.add("item_4");
        linkedList.add("item_5");

        System.out.println("LinkedList items:");
        linkedList.printList();
        System.out.println();

        System.out.println("Reverse LinkedList items:");
        linkedList.printReverseList();
        System.out.println();
    }

    private static void testDoublyLinkedList()
    {
        List<String> doublyLinkedList = new DoublyLinkedList<>();
        doublyLinkedList.add("item_1");
        doublyLinkedList.add("item_2");
        doublyLinkedList.add("item_3");
        doublyLinkedList.add("item_4");
        doublyLinkedList.add("item_5");

        System.out.println("DoublyLinkedList items:");
        doublyLinkedList.printList();
        System.out.println();

        System.out.println("Reverse DoublyLinkedList items:");
        doublyLinkedList.printReverseList();
        System.out.println();
    }

    private static void testRWNodeList()
    {
        RWNode third = new RWNodeImpl("third", null);
        RWNode second = new RWNodeImpl("second", third);
        RWNode first = new RWNodeImpl("first", second);


        System.out.println("RWNode items:");
        RWPrinter.printList(first);
        System.out.println();

        System.out.println("Reverse RWNode items:");
        RWPrinter.printListReverse(first);
        System.out.println();
    }

    private static void testSpesialArray()
    {
        int[] arr = {5, 9, 7, 17, 6, 5, 4, 6};
        System.out.println("Array [" + arrayAsString(arr) + "] is special: " + SpecialArray.isSpecial(arr));
        System.out.println();
    }

    private static String arrayAsString(int[] arr)
    {
        StringBuilder builder = new StringBuilder();
        Arrays.stream(arr).forEach(value -> builder.append(value).append(" "));
        return builder.toString();
    }
}
