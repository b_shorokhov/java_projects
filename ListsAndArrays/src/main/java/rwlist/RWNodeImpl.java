package rwlist;

/**
 * @author: bshorokhov
 */
public class RWNodeImpl implements RWNode
{
    private String name;
    private RWNode next;

    public RWNodeImpl(String name, RWNode next)
    {
        this.name = name;
        this.next = next;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public RWNode getNext()
    {
        return this.next;
    }
}
