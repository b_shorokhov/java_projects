package rwlist;

import java.util.Iterator;

/**
 * @author: bshorokhov
 */
public final class RWPrinter
{
    public static void printList(RWNode node)
    {
        Iterator iterator = new RWNodeIterator(node);
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public static void printListReverse(RWNode node)
    {
        if (node != null)
        {
            printListReverse(node.getNext());
            System.out.println(node.getName());
        }
    }
}
