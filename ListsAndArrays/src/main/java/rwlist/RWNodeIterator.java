package rwlist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author: bshorokhov
 */
public class RWNodeIterator implements Iterator
{
    private RWNode next;

    public RWNodeIterator(RWNode node)
    {
        next = node;
    }

    public boolean hasNext()
    {
        return next != null;
    }

    public String next()
    {
        if (!hasNext())
        {
            throw new NoSuchElementException();
        }

        String result = next.getName();
        next = next.getNext();
        return result;
    }

    public void remove()
    {
        throw new UnsupportedOperationException();
    }
}
