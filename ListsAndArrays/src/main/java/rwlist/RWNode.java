package rwlist;

/**
 * @author: bshorokhov
 */
public interface RWNode
{
    String getName();

    RWNode getNext();
}
