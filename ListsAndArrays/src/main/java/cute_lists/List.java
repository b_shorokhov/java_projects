package cute_lists;

/**
 * @author: bshorokhov
 */
public interface List<Item> extends Iterable<Item>
{
    void add(Item item);

    void remove(Item item);

    void printList();

    void printReverseList();

    void removeAll();

    boolean isEmpty();

    int size();
}
