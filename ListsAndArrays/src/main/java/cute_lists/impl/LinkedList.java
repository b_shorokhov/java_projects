package cute_lists.impl;

import cute_lists.List;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author: bshorokhov
 */
public class LinkedList<Item> implements List<Item>
{
    private RWNode first;
    private int size;

    public LinkedList()
    {
        first = null;
    }

    private class RWNode
    {
        private Item item;
        private RWNode next;

        public RWNode(Item item, RWNode next)
        {
            this.item = item;
            this.next = next;
        }
    }

    private class RWListIterator implements Iterator<Item>
    {

        private RWNode next;

        public RWListIterator()
        {
            next = first;
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public Item next()
        {
            if (!hasNext())
            {
                throw new NoSuchElementException();
            }
            Item result = next.item;
            next = next.next;
            return result;
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    public Iterator<Item> iterator()
    {
        return new RWListIterator();
    }

    public void add(Item item)
    {
        addLast(item);
    }

    public void remove(Item item)
    {
        if (first == null)
        {
            throw new RuntimeException("Impossible to delete, list is empty!");
        }

        if (first.item.equals(item))
        {
            first = first.next;
            size--;
            return;
        }

        RWNode current = first;
        RWNode previous = null;

        while (current != null && !current.item.equals(item))
        {
            previous = current;
            current = current.next;
        }

        if (current == null)
        {
            throw new RuntimeException("Impossible to delete!");
        }

        previous.next = current.next;
        size--;
    }

    public void addFirst(Item item)
    {
        first = new RWNode(item, first);
        size++;
    }

    public void addLast(Item item)
    {
        if (first == null)
        {
            addFirst(item);
        }
        else
        {
            RWNode temp = first;
            while (temp.next != null)
            {
                temp = temp.next;
            }
            temp.next = new RWNode(item, null);
            size++;
        }
    }

    public void removeAll()
    {
        for (Item item : this)
        {
            this.remove(item);
        }
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public int size()
    {
        return size;
    }

    private LinkedList<Item> reverseList()
    {
        LinkedList<Item> reverseList = new LinkedList<Item>();
        for (Item item : this)
        {
            reverseList.addFirst(item);
        }
        return reverseList;
    }

    public void printList()
    {
        for (Item item : this)
        {
            System.out.println(item);
        }
    }

    public void printReverseList()
    {
        for (Item item : reverseList())
        {
            System.out.println(item);
        }
    }
}
