package cute_lists.impl;

import cute_lists.List;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author: bshorokhov
 */
public class DoublyLinkedList<Item> implements List<Item>
{
    private RWNode first;
    private RWNode last;
    private int size;

    private class RWNode
    {

        private Item item;
        private RWNode next;
        private RWNode previous;

        public RWNode(Item item, RWNode previous, RWNode next)
        {
            this.item = item;
            this.previous = previous;
            this.next = next;
        }
    }

    private class RWDoublyListIterator implements Iterator<Item>
    {
        private RWNode next;

        public RWDoublyListIterator()
        {
            next = first;
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public Item next()
        {
            if (!hasNext())
            {
                throw new NoSuchElementException();
            }
            Item result = next.item;
            next = next.next;
            return result;
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    private class RWDoublyListReverseIterator implements Iterator<Item>
    {
        private RWNode next;

        public RWDoublyListReverseIterator()
        {
            next = last;
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public Item next()
        {
            if (!hasNext())
            {
                throw new NoSuchElementException();
            }
            Item result = next.item;
            next = next.previous;
            return result;
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    public Iterator<Item> iterator()
    {
        return new RWDoublyListIterator();
    }

    public Iterator<Item> reverseIterator()
    {
        return new RWDoublyListReverseIterator();
    }

    public void add(Item item)
    {
        addLast(item);
    }

    public void remove(Item item)
    {
        if (isEmpty())
        {
            throw new RuntimeException("Impossible to delete, list is empty!");
        }

        for (RWNode node = first; node != null; node = node.next)
        {
            if (item == null || item.equals(node.item))
            {
                final RWNode next = node.next;
                final RWNode previous = node.previous;

                if (previous == null)
                {
                    first = next;
                }
                else
                {
                    previous.next = next;
                    node.previous = null;
                }

                if (next == null)
                {
                    last = previous;
                }
                else
                {
                    next.previous = previous;
                    node.next = null;
                }

                node.item = null;
            }
        }

        size--;
    }

    public void addFirst(Item item)
    {
        RWNode temp = new RWNode(item, null, first);

        if (first != null)
        {
            first.previous = temp;
        }
        first = temp;
        if (last == null)
        {
            last = temp;
        }

        size++;
    }

    public void addLast(Item item)
    {
        RWNode temp = new RWNode(item, last, null);

        if (last != null)
        {
            last.next = temp;
        }
        last = temp;
        if (first == null)
        {
            first = temp;
        }

        size++;
    }

    public void removeAll()
    {
        for (Item item : this)
        {
            this.remove(item);
        }
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public int size()
    {
        return size;
    }

    public void printList()
    {
        for (Item item : this)
        {
            System.out.println(item);
        }
    }

    public void printReverseList()
    {
        Iterator<Item> reverseIterator = reverseIterator();
        while (reverseIterator.hasNext())
        {
            System.out.println(reverseIterator.next());
        }
    }
}
