package array;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author: bshorokhov
 */
public class SpecialArrayTest
{
    private static final int[] SPECIAL_ARRAY = {5, 9, 7, 17, 6, 5, 4, 6};
    private static final int[] NOT_SPECIAL_ARRAY = {5, 9, 7, 17, 6, 5, 4, 7};
    private static final int[] ARRAY_INSUFFICIENT_ELEMENTS = {5};
    private static final int[] EMPTY_ARRAY = {};

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void isSpecial() throws Exception
    {
        assertTrue(SpecialArray.isSpecial(SPECIAL_ARRAY));
        assertFalse(SpecialArray.isSpecial(NOT_SPECIAL_ARRAY));

        exception.expect(IllegalArgumentException.class);
        assertFalse(SpecialArray.isSpecial(ARRAY_INSUFFICIENT_ELEMENTS));

        exception.expect(IllegalArgumentException.class);
        SpecialArray.isSpecial(EMPTY_ARRAY);
    }
}