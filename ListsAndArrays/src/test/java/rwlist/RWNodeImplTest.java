package rwlist;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author: bshorokhov
 */
public class RWNodeImplTest
{
    private RWNode first;
    private RWNode second;
    private RWNode third;

    @Before
    public void setUp() throws Exception
    {
        third = new RWNodeImpl("third", null);
        second = new RWNodeImpl("second", third);
        first = new RWNodeImpl("first", second);
    }

    @After
    public void tearDown() throws Exception
    {
        first = null;
        second = null;
        third = null;
    }

    @Test
    public void getName() throws Exception
    {
        assertEquals("first", first.getName());
        assertEquals("second", second.getName());
        assertEquals("third", third.getName());
    }

    @Test
    public void getNext() throws Exception
    {
        assertEquals(second, first.getNext());
        assertEquals(third, second.getNext());
        assertEquals(null, third.getNext());
    }

}