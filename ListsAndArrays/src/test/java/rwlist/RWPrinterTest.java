package rwlist;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

/**
 * @author: bshorokhov
 */
public class RWPrinterTest
{

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private RWNode first;
    private RWNode second;
    private RWNode third;

    @Before
    public void setUp() throws Exception
    {
        third = new RWNodeImpl("third", null);
        second = new RWNodeImpl("second", third);
        first = new RWNodeImpl("first", second);

        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() throws Exception
    {
        first = null;
        second = null;
        third = null;

        System.setOut(null);
    }

    @Test
    public void printList() throws Exception
    {
        String expected = "first\nsecond\nthird\n";
        RWPrinter.printList(first);
        assertEquals(expected, outContent.toString());
        RWPrinter.printList(null);
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void printListReverse() throws Exception
    {
        String expected = "third\nsecond\nfirst\n";
        RWPrinter.printListReverse(first);
        assertEquals(expected, outContent.toString());
        RWPrinter.printList(null);
        assertEquals(expected, outContent.toString());
    }

}