package rwlist;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;

/**
 * @author: bshorokhov
 */
public class RWNodeIteratorTest
{
    private RWNode first;
    private RWNode second;
    private RWNode third;
    private Iterator iterator;

    @Before
    public void setUp() throws Exception
    {
        third = new RWNodeImpl("third", null);
        second = new RWNodeImpl("second", third);
        first = new RWNodeImpl("first", second);
        iterator = new RWNodeIterator(first);
    }

    @After
    public void tearDown() throws Exception
    {
        first = null;
        second = null;
        third = null;
        iterator = null;
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void hasNext() throws Exception
    {
        List<String> iteratedElements = new ArrayList<>();
        while (iterator.hasNext())
        {
            iteratedElements.add((String) iterator.next());
        }

        assertEquals(first.getName(), iteratedElements.get(0));
        assertEquals(second.getName(), iteratedElements.get(1));
        assertEquals(third.getName(), iteratedElements.get(2));
    }

    @Test
    public void next() throws Exception
    {
        assertEquals(first.getName(), iterator.next());
        assertEquals(second.getName(), iterator.next());
        assertEquals(third.getName(), iterator.next());

        exception.expect(NoSuchElementException.class);
        assertEquals(null, iterator.next());
    }

    @Test
    public void remove() throws Exception
    {
        exception.expect(UnsupportedOperationException.class);
        iterator.remove();
    }

}