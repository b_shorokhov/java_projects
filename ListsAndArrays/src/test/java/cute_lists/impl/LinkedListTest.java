package cute_lists.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author: bshorokhov
 */
public class LinkedListTest
{
    private static final String NEW_ITEM = "newItem";
    private static final String UNEXISTING_ITEM = "UnexistingItem";

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private LinkedList<String> linkedList = new LinkedList<>();

    @Before
    public void setUp() throws Exception
    {
        linkedList.add("item_1");
        linkedList.add("item_2");
        linkedList.add("item_3");
        linkedList.add("item_4");
        linkedList.add("item_5");

        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() throws Exception
    {
        linkedList.removeAll();
        System.setOut(null);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void iterator() throws Exception
    {
        // testing empty list iterator
        LinkedList<String> emptyLinkedList = new LinkedList<>();
        Iterator<String> emptyListIterator = emptyLinkedList.iterator();

        assertFalse(emptyListIterator.hasNext());

        exception.expect(NoSuchElementException.class);
        emptyListIterator.next();

        // testing list with 1 element iterator
        LinkedList<String> linkedListWithOneItem = new LinkedList<>();
        linkedListWithOneItem.add(NEW_ITEM);
        Iterator listWithOneItemIterator = linkedListWithOneItem.iterator();

        assertTrue(listWithOneItemIterator.hasNext());
        assertEquals(NEW_ITEM, listWithOneItemIterator.next());
        assertFalse(listWithOneItemIterator.hasNext());

        exception.expect(NoSuchElementException.class);
        listWithOneItemIterator.next();

        exception.expect(UnsupportedOperationException.class);
        listWithOneItemIterator.remove();

        //test list with several items
        String[] iteratedArray = new String[]{};
        int arrayIndex = 0;

        Iterator<String> listIterator = linkedList.iterator();
        while (listIterator.hasNext())
        {
            iteratedArray[arrayIndex] = listIterator.next();
            arrayIndex++;
        }

        assertEquals(linkedList.size(), iteratedArray.length);

        for (int i = 0; i < iteratedArray.length; i++)
        {
            assertEquals(iteratedArray[i], "item_" + i);
        }
    }

    @Test
    public void add() throws Exception
    {
        linkedList.add(NEW_ITEM);
        assertEquals(6, linkedList.size());
    }

    @Test
    public void remove() throws Exception
    {
        linkedList.add(NEW_ITEM);
        linkedList.remove(NEW_ITEM);
        assertEquals(5, linkedList.size());

        linkedList.removeAll();
        exception.expect(RuntimeException.class);
        linkedList.remove(NEW_ITEM);

        exception.expect(RuntimeException.class);
        linkedList.remove(UNEXISTING_ITEM);
    }

    @Test
    public void addFirst() throws Exception
    {
        linkedList.addFirst(NEW_ITEM);
        assertEquals(6, linkedList.size());
        assertEquals(NEW_ITEM, linkedList.iterator().next());
    }

    @Test
    public void addLast() throws Exception
    {
        linkedList.addLast(NEW_ITEM);
        assertEquals(6, linkedList.size());

        String last = null;
        for (String item : linkedList)
        {
            last = item;
        }

        assertEquals(NEW_ITEM, last);
    }

    @Test
    public void removeAll() throws Exception
    {
        linkedList.removeAll();
        assertEquals(0, linkedList.size());
    }

    @Test
    public void isEmpty() throws Exception
    {
        assertFalse(linkedList.isEmpty());
        linkedList.removeAll();
        assertTrue(linkedList.isEmpty());
    }

    @Test
    public void size() throws Exception
    {
        assertEquals(5, linkedList.size());
        linkedList.addFirst(NEW_ITEM);
        assertEquals(6, linkedList.size());
        linkedList.remove(NEW_ITEM);
        assertEquals(5, linkedList.size());
        linkedList.removeAll();
        assertEquals(0, linkedList.size());
    }

    @Test
    public void printRWList() throws Exception
    {
        String expected = "item_1\nitem_2\nitem_3\nitem_4\nitem_5\n";
        linkedList.printList();
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void printReverseRWList() throws Exception
    {
        String expected = "item_5\nitem_4\nitem_3\nitem_2\nitem_1\n";
        linkedList.printReverseList();
        assertEquals(expected, outContent.toString());
    }

}