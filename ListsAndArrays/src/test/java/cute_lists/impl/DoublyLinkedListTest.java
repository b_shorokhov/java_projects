package cute_lists.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author: bshorokhov
 */
public class DoublyLinkedListTest
{
    private static final String NEW_ITEM = "newItem";
    private static final String UNEXISTING_ITEM = "UnexistingItem";

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private DoublyLinkedList<String> doublyLinkedList = new DoublyLinkedList<>();

    @Before
    public void setUp() throws Exception
    {
        doublyLinkedList.add("item_1");
        doublyLinkedList.add("item_2");
        doublyLinkedList.add("item_3");
        doublyLinkedList.add("item_4");
        doublyLinkedList.add("item_5");

        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() throws Exception
    {
        doublyLinkedList.removeAll();
        System.setOut(null);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void iterator() throws Exception
    {
        // testing empty list iterator
        DoublyLinkedList<String> emptyDoublyLinkedList = new DoublyLinkedList<>();
        Iterator<String> emptyDoublyListIterator = emptyDoublyLinkedList.iterator();

        assertFalse(emptyDoublyListIterator.hasNext());

        exception.expect(NoSuchElementException.class);
        emptyDoublyListIterator.next();

        // testing list with 1 element iterator
        DoublyLinkedList<String> doublyLinkedListWithOneItem = new DoublyLinkedList<>();
        doublyLinkedListWithOneItem.add(NEW_ITEM);
        Iterator doublyListWithOneItemIterator = doublyLinkedListWithOneItem.iterator();

        assertTrue(doublyListWithOneItemIterator.hasNext());
        assertEquals(NEW_ITEM, doublyListWithOneItemIterator.next());
        assertFalse(doublyListWithOneItemIterator.hasNext());

        exception.expect(NoSuchElementException.class);
        doublyListWithOneItemIterator.next();

        exception.expect(UnsupportedOperationException.class);
        doublyListWithOneItemIterator.remove();

        //test list with several items
        List<String> iteratedElements = new ArrayList<>();

        Iterator<String> listIterator = doublyLinkedList.iterator();
        while (listIterator.hasNext())
        {
            iteratedElements.add(listIterator.next());
        }

        assertEquals(doublyLinkedList.size(), iteratedElements.size());

        for (int i = 0; i < iteratedElements.size(); i++)
        {
            assertEquals(iteratedElements.get(i), "item_" + i);
        }
    }

    @Test
    public void reverseIterator() throws Exception
    {
        // testing empty list iterator
        DoublyLinkedList<String> emptyDoublyLinkedList = new DoublyLinkedList<>();
        Iterator<String> emptyDoublyListReverseIterator = emptyDoublyLinkedList.iterator();

        assertFalse(emptyDoublyListReverseIterator.hasNext());

        exception.expect(NoSuchElementException.class);
        emptyDoublyListReverseIterator.next();

        // testing list with 1 element iterator
        DoublyLinkedList<String> doublyLinkedListWithOneItem = new DoublyLinkedList<>();
        doublyLinkedListWithOneItem.add(NEW_ITEM);
        Iterator doublyListWithOneItemReverseIterator = doublyLinkedListWithOneItem.iterator();

        assertTrue(doublyListWithOneItemReverseIterator.hasNext());
        assertEquals(NEW_ITEM, doublyListWithOneItemReverseIterator.next());
        assertFalse(doublyListWithOneItemReverseIterator.hasNext());

        exception.expect(NoSuchElementException.class);
        doublyListWithOneItemReverseIterator.next();

        exception.expect(UnsupportedOperationException.class);
        doublyListWithOneItemReverseIterator.remove();

        //test list with several items
        List<String> iteratedElements = new ArrayList<>();

        Iterator<String> listIterator = doublyLinkedList.iterator();
        while (listIterator.hasNext())
        {
            iteratedElements.add(listIterator.next());
        }

        assertEquals(doublyLinkedList.size(), iteratedElements.size());

        for (int i = 0; i < iteratedElements.size(); i++)
        {
            assertEquals(iteratedElements.get(iteratedElements.size() - i + 1), "item_" + i);
        }
    }

    @Test
    public void add() throws Exception
    {
        doublyLinkedList.add(NEW_ITEM);
        assertEquals(6, doublyLinkedList.size());
    }

    @Test
    public void remove() throws Exception
    {
        doublyLinkedList.add(NEW_ITEM);
        doublyLinkedList.remove(NEW_ITEM);
        assertEquals(5, doublyLinkedList.size());

        doublyLinkedList.removeAll();
        exception.expect(RuntimeException.class);
        doublyLinkedList.remove(NEW_ITEM);

        exception.expect(RuntimeException.class);
        doublyLinkedList.remove(UNEXISTING_ITEM);
    }

    @Test
    public void addFirst() throws Exception
    {
        doublyLinkedList.addFirst(NEW_ITEM);
        assertEquals(6, doublyLinkedList.size());
        assertEquals(NEW_ITEM, doublyLinkedList.iterator().next());
    }

    @Test
    public void addLast() throws Exception
    {
        doublyLinkedList.addLast(NEW_ITEM);
        assertEquals(6, doublyLinkedList.size());

        String last = null;
        for (String item : doublyLinkedList)
        {
            last = item;
        }

        assertEquals(NEW_ITEM, last);
    }

    @Test
    public void removeAll() throws Exception
    {
        doublyLinkedList.removeAll();
        assertEquals(0, doublyLinkedList.size());
    }

    @Test
    public void isEmpty() throws Exception
    {
        assertFalse(doublyLinkedList.isEmpty());
        doublyLinkedList.removeAll();
        assertTrue(doublyLinkedList.isEmpty());
    }

    @Test
    public void size() throws Exception
    {
        assertEquals(5, doublyLinkedList.size());
        doublyLinkedList.add(NEW_ITEM);
        assertEquals(6, doublyLinkedList.size());
        doublyLinkedList.remove(NEW_ITEM);
        assertEquals(5, doublyLinkedList.size());
        doublyLinkedList.removeAll();
        assertEquals(0, doublyLinkedList.size());
    }

    @Test
    public void printRWList() throws Exception
    {
        String expected = "item_1\nitem_2\nitem_3\nitem_4\nitem_5\n";
        doublyLinkedList.printList();
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void printReverseRWList() throws Exception
    {
        String expected = "item_5\nitem_4\nitem_3\nitem_2\nitem_1\n";
        doublyLinkedList.printReverseList();
        assertEquals(expected, outContent.toString());
    }

}